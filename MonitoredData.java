import java.text.DateFormat;
import java.util.Date;

public class MonitoredData {

    private String activity;
    private String startTime, endTime;

    public MonitoredData(String startTime, String endTime, String activity) {
        this.startTime = startTime;
        this.activity = activity;
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String toString(){
        return startTime.toString()+" "+endTime.toString()+" "+activity.toString();
    }

    public Integer getStartDay(){
        String[] array=startTime.split(" ");
        String[] array1=array[0].split("-");
        return Integer.parseInt(array1[2]);
    }

    public Integer getEndDay(){
        String[] array=endTime.split(" ");
        String[] array1=array[0].split("-");
        return Integer.parseInt(array1[2]);
    }

    public Integer getStartMonth(){
        String[] array=startTime.split(" ");
        String[] array1=array[0].split("-");
        return Integer.parseInt(array1[1]);
    }

    public Integer getEndMonth(){
        String[] array=endTime.split(" ");
        String[] array1=array[0].split("-");
        return Integer.parseInt(array1[1]);
    }

    public Integer getHourStart(){
        String[] array=startTime.split(" ");
        String[] array1=array[1].split(":");
        return Integer.parseInt(array1[0]);
    }
    public Integer getMinStart(){
        String[] array=startTime.split(" ");
        String[] array1=array[1].split(":");
        return Integer.parseInt(array1[1]);
    }

    public Integer getSecStart(){
        String[] array=startTime.split(" ");
        String[] array1=array[1].split(":");
        return Integer.parseInt(array1[2]);
    }

    public Integer getHourEnd(){
        String[] array=endTime.split(" ");
        String[] array1=array[1].split(":");
        return Integer.parseInt(array1[0]);
    }
    public Integer getMinEnd(){
        String[] array=endTime.split(" ");
        String[] array1=array[1].split(":");
        return Integer.parseInt(array1[1]);
    }

    public Integer getSecEnd(){
        String[] array=endTime.split(" ");
        String[] array1=array[1].split(":");
        return Integer.parseInt(array1[2]);
    }

    public int timeInSec(){
        int sec=getSecEnd()-getSecStart();
        int min=getMinEnd()-getMinStart();
        int hour=getHourEnd()-getHourStart()+(getEndDay() - getStartDay())*24 + 24*30*(getEndMonth()- getStartMonth());
        sec+=min*60+hour*3600;
        return sec;
    }
}
