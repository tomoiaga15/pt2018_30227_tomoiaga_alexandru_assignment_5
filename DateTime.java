public class DateTime {

    private int hour, min, sec;

    public DateTime(int hour, int min, int sec){
        this.hour=hour;
        this.min=min;
        this.sec=sec;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getSec() {
        return sec;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }

    @Override
    public String toString() {
        return "DateTime{" +
                "hour=" + hour +
                ", min=" + min +
                ", sec=" + sec;
    }
}
