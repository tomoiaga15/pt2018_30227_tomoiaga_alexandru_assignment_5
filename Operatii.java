import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Operatii {

    public List<MonitoredData> readData() throws IOException {
        return Files.lines(Paths.get("Activities.txt")).map(s -> {
            String[] array = s.split("\t\t");
            return new MonitoredData(array[0], array[1], array[2]);
        }).collect(Collectors.toList());
    }

    public long taskOne(List<MonitoredData> list) {
        return list.stream().map(s -> {
            return s.getStartDay();
        }).distinct().count();
    }

    public void taskTwo(List<MonitoredData> list) throws IOException {
        Map<String, Long> hm = interTask(list);
        Files.write(Paths.get("Task2.txt"), Collections.singleton(hm.toString()));
    }

    public void taskThree(List<MonitoredData> list) throws IOException {
        Map<Integer, Map<String, Long>> hm = list.stream().collect(Collectors.groupingBy(MonitoredData::getStartDay, Collectors.groupingBy(s -> s.getActivity(), Collectors.counting())));
        Files.write(Paths.get("Task3.txt"), Collections.singleton(hm.toString()));
    }


    public void taskFour(List<MonitoredData> list) throws IOException {
        Map<String, DateTime> hm = list.stream().
                collect(Collectors.
                        groupingBy(MonitoredData::getActivity, Collectors.
                                summingInt(MonitoredData::timeInSec))).
                entrySet().
                stream().
                filter(s -> s.getValue() > 36000).
                collect(Collectors.
                        toMap(s -> s.getKey(), s -> new DateTime(s.getValue() / 3600, (s.getValue() % 3600) / 60, s.getValue() % 60)));
        Files.write(Paths.get("Task4.txt"), Collections.singleton(hm.toString()));
    }

    public void taskFive(List<MonitoredData> list) throws IOException {
        Map<String, Long> hm1 = interTask(list);
        Map<String, Long> hm2 = list.stream().filter(s->s.timeInSec()>=300).map(s -> {
            return s.getActivity();
        }).collect(Collectors.groupingBy(s -> s,
                Collectors.counting()));
        List<String> stringList = hm2.
                entrySet().
                stream().
                filter(s->s.getValue()*100>=90*hm1.get(s.getKey())).
                map(s->{
            return s.getKey();
        }).collect(Collectors.toList());

        Files.write(Paths.get("Task5.txt"), Collections.singleton(stringList.toString()));
    }

   public List<DateTime> task(List<MonitoredData> list)
    {
        return list.stream().map(s -> {
            return toDateTime(s);
        }).collect(Collectors.toList());
    }

    public Map<String, Long> interTask(List<MonitoredData> list){
        return list.stream().map(s -> {
            return s.getActivity();
        }).collect(Collectors.groupingBy(s -> s,
                Collectors.counting()));
    }

    public DateTime toDateTime(MonitoredData mD) {

        int sec = 60 + mD.getSecEnd() - mD.getSecStart();
        int min = 59 + mD.getMinEnd() - mD.getMinStart()+sec/60;
        int hour =-1+ mD.getHourEnd()- mD.getHourStart()+min/60+ (mD.getEndDay() - mD.getStartDay())*24 + 24*30*(mD.getEndMonth()- mD.getStartMonth());
        sec%=60;
        min%=60;
        return new DateTime(hour, min, sec);
    }
}
