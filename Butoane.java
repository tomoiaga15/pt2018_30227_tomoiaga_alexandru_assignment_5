import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Butoane implements ActionListener {
    private int id;
    private JTextField s1;

    public Butoane(int id, JTextField s1) {
        this.id = id;
        this.s1 = s1;
    }

    public Butoane(int id) {
        this.id = id;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        Operatii o=new Operatii();
            List<MonitoredData> list=new ArrayList<>();
        try {
            list=o.readData();
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch(id) {
            case 0:
                int i=(int)o.taskOne(list);
                s1.setText(String.valueOf(i));
                break;
            case 1:
                try {
                    o.taskTwo(list);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    o.taskThree(list);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                try {
                    o.taskFour(list);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 4:
                try {
                    o.taskFive(list);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
