import java.awt.*;
import javax.swing.*;

class View extends JFrame {


    JPanel panel1, panel2;
    JLabel l1;
    JTextField tf1;
    JButton b1, b2, b3, b4, b5;

    public View(String nume) {
        setTitle(nume);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(480, 180);

        panel1 = new JPanel();
        panel2 = new JPanel();

        l1 = new JLabel("Numar zile:");

        tf1 = new JTextField("-");

        tf1.setColumns(10);


        panel1.setLayout(new FlowLayout());
        panel2.setLayout(new FlowLayout());

        b1 = new JButton("Task1");
        b2 = new JButton("Task2");
        b3 = new JButton("Task3");
        b4 = new JButton("Task4");
        b5 = new JButton("Task5");

        panel1.add(b1);
        panel1.add(b2);
        panel1.add(b3);
        panel1.add(b4);
        panel1.add(b5);
        panel2.add(l1);
        panel2.add(tf1);

        b1.addActionListener(new Butoane(0, tf1));
        b2.addActionListener(new Butoane(1));
        b3.addActionListener(new Butoane(2));
        b4.addActionListener(new Butoane(3));
        b5.addActionListener(new Butoane(4));

        JPanel p = new JPanel();
        p.add(panel1);
        p.add(panel2);
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        setContentPane(p);
        setVisible(true);
    }

    public static void main(String args[]) {
        new View("Pagina principala");
    }

}